import Foundation
import UIKit

let name = "Roman"

for letter in name {
    print("Give ma a \(letter)")
}

let letter = name[name.index(name.startIndex, offsetBy: 3)]

// add strings a behavior of array
extension String {
    subscript(i: Int) -> String {
        return String(self[index(startIndex, offsetBy: i)])
    }
}

let letter2 = name[0]











let password = "123456"
password.hasPrefix("123")
password.hasSuffix("456")

extension String {
    func deletingPrefix(_ prefix: String) -> String {
        guard self.hasPrefix(prefix) else { return self }
        return String(self.dropFirst(prefix.count))
    }
    
    func deletingSuffix(_ suffix: String) -> String {
        guard self.hasSuffix(suffix) else { return self }
        return String(self.dropLast(suffix.count))
    }
}

var str = "12345676543".deletingSuffix("543")

let weather = "it's going to rain"
//print(weather.capitalized)

extension String {
    var capitalizedFirst: String {
        guard let firstLetter = self.first else { return "" }
        return firstLetter.uppercased() + self.dropFirst()
    }
}

weather.capitalizedFirst















let input = "Swift is like Obj-C without C"
input.contains("Swift")

let languages = ["Python", "Ruby", "Swift"]
languages.contains("Ruby")

extension String {
    func contains(of array: [String]) -> Bool {
        for item in array {
            if self.contains(item) {
                return true
            }
        }
        
        return false
    }
}

input.contains(of: languages)

languages.contains(where: input.contains)







let string = "This is a test string"
let attributedString = NSMutableAttributedString(string: string)

attributedString.addAttribute(.font,
                              value: UIFont.systemFont(ofSize: 8),
                              range: NSRange(location: 0,
                                             length: 4))
attributedString.addAttribute(.font,
                              value: UIFont.systemFont(ofSize: 16),
                              range: NSRange(location: 5,
                                             length: 2))
attributedString.addAttribute(.font,
                              value: UIFont.systemFont(ofSize: 24),
                              range: NSRange(location: 8,
                                             length: 1))
attributedString.addAttribute(.font,
                              value: UIFont.systemFont(ofSize: 32),
                              range: NSRange(location: 10,
                                             length: 4))
attributedString.addAttribute(.font,
                              value: UIFont.systemFont(ofSize: 40),
                              range: NSRange(location: 15,
                                             length: 6))
//let atributes: [NSAttributedString.Key: Any] = [
//    .foregroundColor: UIColor.white,
//    .backgroundColor: UIColor.red,
//    .font: UIFont.boldSystemFont(ofSize: 36)
//]
//
//let attributedString = NSAttributedString(string: string, attributes: atributes)


// MARK: - Challenge
// 1
extension String {
    func withPrefix(_ prefix: String) -> String {
        guard self.hasPrefix(prefix) == false else { return self }
        return prefix + self
    }
}

"pet".withPrefix("car")
// 2
var numbers = ["1","2","3","4","5","6","7","8","9","0"]
extension String {
    var isNumeric: Bool {
        if numbers.contains(where: self.contains) {
            return true
        }
        
        return false
    }
}

"lolo23".isNumeric

// 3
extension String {
    func returnLines() -> [String] {
        var lines: [String] = []
        for letter in self {
            if letter == "\""{
                lines.append(String(letter))
            }
        }
        return lines
    }
}

"fjdnfj\"f\"f\"\"ffdfd\"fdf".returnLines()
